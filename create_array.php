<?php


//Функция возвращает массив по заданным параметрам $x и $y
//function create_array($x,$y) {

//    $x = intval($x, 10);
//    $y = intval($y, 10);

    $x = 5;
    $y = 6;


    //Создание массива с нулями

    for ($i_for_y = 0; $i_for_y < $y; $i_for_y++) {
        for ($i_for_x = 0; $i_for_x < $x; $i_for_x++){
            $array_sheet[$i_for_y][$i_for_x] = 0;
        }
    }


    //Создание массива бомб

    $my_array_mines= [];
    $i_for_mines = 0;
    $x_y = $x * $y;

    while(intdiv($x_y, 16)*3 > $i_for_mines) {
        $buffer_number = rand(0, ($x_y - 1));
        if ((in_array($buffer_number, $my_array_mines)) == false) {
            $my_array_mines[$i_for_mines] = $buffer_number;
            $i_for_mines++;
        }
    }


    //Накладываем массив бомб на нулевой массив

    for ($yy = 0, $i_for_mines = 0; $yy < $y; $yy++) {

        for ($xx = 0; $xx < $x; $xx++, $i_for_mines++) {

            $a = in_array($i_for_mines, $my_array_mines);
            if ($a == true) { $array_sheet[$yy][$xx] = "*"; }

        }
    }


    //Создаем доп элементы массива для подсчета коэф.

    $array_sheet[-1] = range(0, $x);
    $array_sheet[-1][-1] = 0;
    $array_sheet[$y] = range(0, $x);
    $array_sheet[$y][-1] = 0;

    foreach ($array_sheet as &$item) {
        $item[-1] = 0;
        $item[$x] = 0;
    }


    //Добавляем вокруг каждой бомбы 1

    for ($yy=0; $yy < $y; $yy++) {
        for ($xx = 0; $xx < $x; $xx++) {
            if ($array_sheet[$yy][$xx] === "*") {
                $array_sheet[$yy-1][$xx-1]++;
                $array_sheet[$yy-1][$xx]++;
                $array_sheet[$yy-1][$xx+1]++;
                $array_sheet[$yy][$xx-1]++;
                $array_sheet[$yy][$xx+1]++;
                $array_sheet[$yy+1][$xx-1]++;
                $array_sheet[$yy+1][$xx]++;
                $array_sheet[$yy+1][$xx+1]++;
            }
        }
    }


    //Убираем доп элементы массива после подсчета коэф.

    unset($array_sheet[-1]);
    unset($array_sheet[-1][-1]);
    unset($array_sheet[$y]);
    unset($array_sheet[$y][-1]);
    foreach ($array_sheet as &$item) {
        unset($item[-1]);
        unset($item[$x]);
    }

    print_r($array_sheet);
//    return $array_sheet;
//}